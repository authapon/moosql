package moosql

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

var (
	dbServer = ""
)

func SetDBServer(dbServe string) {
	dbServer = dbServe
}

func GetSQL() (*sql.DB, error) {
	con, err := sql.Open("mysql", dbServer)
	if err != nil {
		return con, err
	}
	err = con.Ping()
	if err != nil {
		return con, err
	}

	return con, nil
}
